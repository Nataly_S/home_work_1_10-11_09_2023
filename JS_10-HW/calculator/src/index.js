/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

let displayInp = document.querySelector('.display > input');
let span = document.getElementById('span');

let op1 = '',
  op2 = '',
  operator = '',

  res = '',
  fin = false,

  memoryArr = [],
  memoryRes = '',
  memoryFlag = false;

// counter = 0,
// doubleClicked = true;

console.log('op1 : ' + op1, "op2 : " + op2, 'operator : ' + operator);

function resetOperands() {
  op1 = '';
  op2 = '';
  operator = '';
  res = '';
  fin = false;
  displayInp.value = '';
}

function culc(op1, op2, operator) {

  switch (operator) {

    case '+':
      // displayInp.value = (parseFloat(op1) + parseFloat(op2));
      console.log(parseFloat(op1) + parseFloat(op2));
      return parseFloat(op1) + parseFloat(op2);

    case '-':
      console.log(parseFloat(op1) - parseFloat(op2));
      return parseFloat(op1) - parseFloat(op2);

    case '*':
      console.log(parseFloat(op1) * parseFloat(op2));
      return parseFloat(op1) * parseFloat(op2);

    case '/':

      if (op2 === '0') {
        console.log("Error");
        displayInp.value = "Error";
        console.log('op1 : ', op1, 'op2 : ', op2, 'operator : ', operator);
        resetOperands();
        return;
      }
      else if (op2 !== '0') {
        console.log(parseFloat(op1) / parseFloat(op2));
        return parseFloat(op1) / parseFloat(op2);
      }
  }
}

function memory(op1, op2, res) {

  if (res !== '') {
    memoryRes = res;

    memoryArr.shift(memoryRes);
    memoryArr.push(memoryRes);
    displayInp.value = memoryRes;

    console.log('memoryRes : ', memoryRes);
    console.log('memoryArr : ', memoryArr);

    console.log(' op1 = ', op1, ' op2 = ', op2, ' res = ', res);
  }
}

document.querySelector('.keys').addEventListener('click', function (e) {

  if (e.target.classList.contains('black')) {

    if (e.target.value === 'C') {

      resetOperands();

      document.getElementById('eq').disabled = true;
      span.style.display = 'none';// memoryArr.shift(memoryRes);

      console.log('op1 : ' + op1 + 'op2 : ' + op2 + 'operator : ' + operator);
    }

    if (e.target.value !== 'C') {

      if ((op2 === '' && operator === '') || (memoryFlag === true && operator === '') || (op1 === 0)) { //&& sign !== true) { //|| (memoryFlag === true)) { //&& memoryFlag === true) { //|| (op2 === null && sign !== true) || (op1 === null && sign === true)) { //|| (memoryArr !== null && op1 === 0 && sign !== true)) {

        op1 += e.target.value;
        memoryRes = op1;

        displayInp.value = op1;
        span.style.display = 'none';
      }
      else if (op1 !== '' && op2 !== '' && fin) {
        op2 = e.target.value;
        fin = false;
        displayInp.value = op2;
      }
      else if ((op1 !== '' && operator !== '') || (memoryFlag === true && operator !== '')) {
        op2 += e.target.value;
        memoryRes = op2;

        displayInp.value = op2;
        span.style.display = 'none';
        document.getElementById('eq').disabled = false;
      }

      console.log('op1 : ', op1, 'op2 : ', op2, 'operator : ', operator)
      return;
    }
  }

  if ((e.target.classList.contains('pink'))) {
    operator = true;
    operator = e.target.value;
    console.log('target = ', e.target.value);
  }

  if (e.target.classList.contains('orange')) {
    if (fin) {
      // e.preventDefault();
      return;
    }
    fin = true;
    res = culc(op1, op2, operator); //.toFixed()

    if (res === Infinity || res === undefined) {

      res = '';
      op1 = res;
      displayInp.value = 'Error';
      console.log("---");
      return;
    }
    else if (res !== Infinity || res !== undefined) {
      op1 = res;
      console.log("+")
    }
    displayInp.value = op1;
    // fin = false;
  }

  if (e.target.classList.contains('gray')) {

    if (e.target.value === 'm+') {

      memory(op1, op2, res);

      resetOperands();
      displayInp.value = memoryRes;
      span.style.display = 'block';


      memoryFlag = true;
      console.log('op1 :', op1, 'op2 :', op2, 'operator :', operator, 'res :', res, 'memoryRes :', memoryRes, 'memoryArr :', memoryArr);
      // document.getElementById('eq').disabled = true;
    }

    if (e.target.value === 'm-') {

      if (memoryFlag === false) {
        e.preventDefault();
      }

      resetOperands();
      memoryArr.shift(memoryRes);

      displayInp.value = '';//memoryArr[0]
      span.style.display = 'block';
      // document.getElementById('eq').disabled = true;
    }

    if (e.target.value === 'mrc') {
      console.log('memoryArr : ', memoryArr[0]);
      console.log('memoryRes : ', memoryRes);

      if (memoryArr[0] === undefined) {
        displayInp.value = '';
      }
      if (memoryArr[0] !== undefined) {
        displayInp.value = memoryArr[0];

        op1 = memoryArr[0];
      }
      span.style.display = 'none';
    }
  }
})
