//завдання 1-ше Секундомір
const get = (className) => document.getElementsByClassName(className);

let black = get('black'),
  stopwatch_control = get('stopwatch-control'),
  stopwatch_display = get('stopwatch-display'),
  container_stopwatch = get('container-stopwatch'),

  startButton = stopwatch_control[0].firstElementChild,
  stopButton = stopwatch_control[0].firstElementChild.nextElementSibling,
  resetButton = stopwatch_control[0].lastElementChild,

  interval,
  flag = false,//не було натиснуто

  counter = 0,
  counterMin = 0,
  counterHours = 0,

  hours = stopwatch_display[0].firstElementChild,
  minutes = stopwatch_display[0].firstElementChild.nextElementSibling,
  seconds = stopwatch_display[0].lastElementChild;

removeRed = () => {
  black[0].classList.remove('red');
}

removeGreen = () => {
  black[0].classList.remove('green');
}

removeSilver = () => {
  black[0].classList.remove('silver');
}

startButton.onclick = () => {
  removeRed();
  removeSilver();
  // stopwatch_display[0][0].classList.remove('green');
  // console.log(stopwatch_display[0].children);
  black[0].classList.add('green');
  if (!flag) {
    interval = setInterval(timer, 100);//1000 мілісекунд = 1 секунда, 10 -для швидшого тестування
    flag = true;
  }
}

stopButton.onclick = () => {
  removeGreen();
  removeSilver();
  black[0].classList.add('red');
  clearInterval(interval);
  flag = false;
}

resetButton.onclick = () => {
  removeRed();
  removeGreen();
  black[0].classList.add('silver');
  //-----------------------------------

  seconds.innerText = '00';
  minutes.innerText = '00';
  hours.innerText = '00';

  counter = 0;
  counterMin = 0;
  counterHours = 0;
}

let timer = () => {
  counter++;
  if (counter <= 59) {
    seconds.innerText = counter;

    if (counter < 10) {
      seconds.innerText = `0${counter}`;
    }
    // console.log(`counter = ${counter}`);
  }

  else if (counter >= 59) {
    counter = 0;
    // console.log(`counter = ${counter}`);

    if (counter <= 59) {
      counterMin++;
      minutes.innerText = counterMin;

      if (counterMin < 10) {
        minutes.innerText = `0${counterMin}`;
      }
      // console.log(`counter = ${counter}`);
      // console.log(`counterMin = ${counterMin}`);
    }
  }

  if (counterMin >= 59) {
    counterMin = 0;

    if (counterMin <= 59) {
      counterHours++;
      hours.innerText = counterHours;

      if (counterHours < 10) {
        hours.innerText = `0${counterHours}`;
      }
      else if (counterHours <= 2) {
        counterHours = 0;// ??як зробити щоб після певної кількості годин таймер спинявся
        clearInterval(interval);
      }
      // console.log(`counter = ${counter}`);
      // console.log(`counterMin = ${counterMin}`);
      // console.log(`counterHours = ${counterHours}`);
    }
  }
}

//завдання 2-ге на регулярні вирази

let div_message = document.querySelector('form > div > div');
let phone_conteiner = document.querySelector('form div');

let input = document.createElement('input');
input.type = 'tel';
input.id = 'phone_input';
input.placeholder = '093-123-45-67';
phone_conteiner.append(input);

let button = document.createElement('button');
button.type = 'button';
button.id = 'button';
button.innerText = 'Підтвердити';
button.style.fontSize = '1rem';
button.style.width = '150px';
button.style.height = '35px';
button.style.margin = '0 auto';
phone_conteiner.append(button);

div_message.style.color = 'red';

//-------------------------------------------------------

let pattern = /^0[0-9]{2}\-[0-9]{3}\-[0-9]{2}\-[0-9]{2}$/;
// console.log(pattern);

button.onclick = () => {
  // console.log(input.value);
  if (pattern.test(input.value)) {

    input.value = null;
    input.style.backgroundColor = 'green';
    setTimeout(document.location = 'https://origamistudy.com/motyvujuchi-cytaty-dlja-dosjagnennja-uspihu/', 3000);
    // console.log(pattern.test(input.value));
  }
  else {
    div_message.textContent = 'Спробуйте ще раз.';
  }
}