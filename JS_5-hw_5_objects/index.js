// Об'єкти 
// +4.Даний рядок типу 'var_text_hello'. Зробіть із нього текст 'VarTextHello'.

let str_ = 'var_text_hello';
let strToUpperCase = str_[0].toUpperCase();
let strTheSameCharacters = str_[1].slice() + str_[2].slice();
    // let deleteChar = str[3].toString.splice();

let strToUpperCase1 = str_[4].toUpperCase();
let strTheSameCharacters1 = str_[5].slice() + str_[6].slice() + str_[7].slice();
    // let deleteChar1 = str[8].toString.splice();

let strToUpperCase2 = str_[9].toUpperCase();
let strTheSameCharacters2 = str_[10].slice() + str_[11].slice() + str_[12].slice() + str_[13].slice();

let newStr = strToUpperCase + strTheSameCharacters + strToUpperCase1 + strTheSameCharacters1 + strToUpperCase2 + strTheSameCharacters2;

document.write(`4-те завдання : ${newStr}`);
document.write(`<br><hr/>`);

//4-те завдання - циклом

let str1 = 'var_text_hello';
function wordsToUpperCase() {
    
    let newStr1 = str1.split("_");

    for (let i = 0; i < str1.length; i++) {

        newStr1[i] = newStr1[i][0].toUpperCase() + newStr1[i].slice(1);// ? newStr1 is underfined - TypeError: ..
        //can't access property 0, newStr1[i] is undefined Paused on exception
        //  can't access property "split", str1 is undefined ?
    }
    let res1 = newStr1.join(" ");
    // return res1;
    console.log(res1);
    // document.write(res1);
}
// wordsToUpperCase();
// document.write(`4-те завдання циклом : ${res1}`);
// document.write(`</br><hr/>`);

//4-те завдання - метод map() та callback функція

// const str = 'var_text_hello_ttt';

// function upperCaseMapFunc(func) {
//     const parts = str.split('_');//метод split() розділяє - в даному випадку забирає знак вказаний в аргументі

//     const upperCaseParts = parts.map(item => { return item[0].toUpperCase() + item.slice(1); }); //метод map()перебирає кожен елемент рядка - ці елементи вже попередньо розділені методом split() і повертає перший елемент рядка у верх.регістрі, а наступні символи метод slice() копіює 

    // const result = upperCaseParts.join('');

    // console.log(upperCaseParts);
    // document.write(upperCaseParts);
// }
// upperCaseMapFunc();
// document.write(result);

//4-те завдання - метод map()---- 

// function upperCaseMapFunc_(str_) {
// const str_ = 'var_text_hello_nuew';
//     const parts = str_.split('_');

//     // console.log(upperCaseParts);
//     // document.write(upperCaseParts_);
// }
// let upperCaseParts_ = upperCaseMapFunc_.map(item => {
//     let result_ = item[0].toUpperCase()+item.slice(1);
//     return result_;
//     // console.log(result_);
//     });

//     //item => {return item[0].toUpperCase() + item.slice(1);}

//     console.log(result_);
// upperCaseMapFunc_();


/* +5.Функція ggg приймає 2 параметри: анонімну функцію, яка повертає 3 та анонімну функцію, яка
повертає 4. Поверніть результатом функції ggg суму 3 та 4.*/

// об'єкт містить дві анонімні функції
var obj = {
    func1: function () { return 3 },
    func2: function () { return (4); }
};
// var res = obj.func1() + obj.func2();
// document.write(res);//працює

let resAdd;
function ggg(x, y) {
    resAdd = obj.func1() + obj.func2();
    // return resAdd;
    // console.log(resAdd);
};
ggg();//виклик функції
document.write(`5-те завданя : ${resAdd}`);//виводить суму чисел
document.write(`<br><hr/>`);

/* +6.Створіть об'єкт криптокошилок. У гаманці має зберігатись ім'я власника, кілька валют Bitcoin, Ethereum Stellar і в кожній валюті додатково є ім'я валюти, логотип, кілька монет та курс на сьогоднішній день день. Також в об'єкті гаманець є метод при виклику якого він приймає ім'я валюти та виводить на сторінку  інформацію."Доброго дня, ... ! На вашому балансі (Назва валюти та логотип) залишилося N монет, якщо ви сьогодні продасте їх те, отримаєте ...грн. */

let objWallet = {
    name: "Nataliia",
    currencyBitcoin: {
        name: "Bitcoin",
        logo: "<img src='./img/Bitcoin.png'>",
        quantity: 1,
        exchange_UAH: 1027784.37,
    },
    currencyEthereum: {
        name: "Ethereum",
        logo: "<img src='./img/Ethereum.png'>",
        quantity: 1,
        exchange_UAH: 1541936836.59,
    },
    currencyStellar: {
        name: "Stellar",
        logo: "<img src='./img/Stellar.png'>",
        quantity: 100,
        exchange_UAH: 390.84,
    },
    call: function () {
        // document.getElementById("div2").innerHTML = (`Доброго дня ${this.name}! На вашому балансі ${objWallet.currencyStellar.name} ${objWallet.currencyStellar.logo} залишилося ${objWallet.currencyStellar.quantity} монет, якщо ви сьогодні продасте їх то отримаєте ${objWallet.currencyStellar.exchange_UAH} грн.`);
        document.write(`6-те завдання : Доброго дня ${this.name}! На вашому балансі ${objWallet.currencyStellar.name} ${objWallet.currencyStellar.logo} залишилося ${objWallet.currencyStellar.quantity} монет, якщо ви сьогодні продасте їх то отримаєте ${objWallet.currencyStellar.exchange_UAH} грн.`)
    }
}
objWallet.call();//працює
document.write(`</br><hr/>`);

// document.write(objWallet.call);//не потрібно, дублює вивід даних
// console.log(objWallet[call]);//не потрібно, дублює вивід

/* +7.Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.
*/

let salaryPerMonth;

let worker = {
    name: "Nata",
    surname: "Alface",
    rate: 27,
    days: 21,
    getSalary: function () {
        salaryPerMonth = this.rate * this.days;
        document.write(`7-ме завдання : ${salaryPerMonth}`);
        // document.getElementById("div1").innerHTML = `<h4>Salary per month is ${salaryPerMonth}</h4>`;
        // return;
    }
}
worker.getSalary();//працює
document.write(`</br><hr/>`);

// document.write(worker.getSalary);//не потрібно, дублює вивід даних
// console.log(worker.getSalary);//не потрібно, дублює вивід

/* 8.Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.
 */

let string = 'spring is already came';

const MyString = {
    reverse: function (string) {
        const stringReverse = string.split(' ')+string.reverse()+string.join('');// ? Uncaught TypeError: can't access property "split", string is undefined
        // return stringReverse;
        console.log(stringReverse);
    },
    ucFirst: function (string) {
        const stringToUpperCase = string[0].toUpperCase() + string.slice(1);// ? Uncaught TypeError: can't access property "split", string is undefined
        document.write(stringToUpperCase);
    },
    ucWords: function () {
        // let string = 'spring is already came';
        let words = string.split(' ');// ? Uncaught TypeError: can't access property "split", string is undefined
        let result;
        for (let i = 0; i < words.length; i++) {
            words[i] = words[i][0].toUpperCase() + words[i].slice(1);
        }
        result = words.join(' ');

        // console.log(result);
        document.write(result);
    }
}
MyString.reverse();
document.write(`8-ме завдання reverse : ${stringReverse}`);
document.write(`</br><hr/>`);

// MyString.ucFirst();
// document.write(`8-ме завдання ucFirst : ${MyString.ucFirst()}`);
document.write(`</br><hr/>`);

// MyString.ucWords(string1);
// document.write(result);

