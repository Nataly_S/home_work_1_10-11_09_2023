//завдання 1-ше Секундомір
const get = (className) => document.getElementsByClassName(className);

let black = get('black'),
  stopwatch_control = get('stopwatch-control'),
  stopwatch_display = get('stopwatch-display'),
  container_stopwatch = get('container-stopwatch'),

  startButton = stopwatch_control[0].firstElementChild,
  stopButton = stopwatch_control[0].firstElementChild.nextElementSibling,
  resetButton = stopwatch_control[0].lastElementChild,

  interval,
  flag = false,//не було натиснуто

  counter = 0,
  counterMin = 0,
  counterHours = 0,

  hours = stopwatch_display[0].firstElementChild,
  minutes = stopwatch_display[0].firstElementChild.nextElementSibling,
  seconds = stopwatch_display[0].lastElementChild;

removeRed = () => {
  black[0].classList.remove('red');
}

removeGreen = () => {
  black[0].classList.remove('green');
}

removeSilver = () => {
  black[0].classList.remove('silver');
}

startButton.onclick = () => {
  removeRed();
  removeSilver();
  // stopwatch_display[0][0].classList.remove('green');
  // console.log(stopwatch_display[0].children);
  black[0].classList.add('green');
  if (!flag) {
    interval = setInterval(timer, 10);//1000 мілісекунд = 1 секунда, 10 -для швидшого тестування
    flag = true;
  }
}

stopButton.onclick = () => {
  removeGreen();
  removeSilver();
  black[0].classList.add('red');
  clearInterval(interval);
  flag = false;
}

resetButton.onclick = () => {
  removeRed();
  removeGreen();
  black[0].classList.add('silver');
  seconds.innerText = '00';//? як обнулити дані 
  minutes.innerText = '00';
  hours.innerText = '00';
}

let timer = () => {
  counter++;
  if (counter <= 59) {
    seconds.innerText = counter;

    if (counter < 10) {
      seconds.innerText = `0${counter}`;
    }
    // console.log(`counter = ${counter}`);
  }

  else if (counter >= 59) {
    counter = 0;
    // console.log(`counter = ${counter}`);

    if (counter <= 59) {
      counterMin++;
      minutes.innerText = counterMin;

      if (counterMin < 10) {
        minutes.innerText = `0${counterMin}`;
      }
      // console.log(`counter = ${counter}`);
      // console.log(`counterMin = ${counterMin}`);
    }
  }

  if (counterMin >= 59) {
    counterMin = 0;

    if (counterMin <= 59) {
      counterHours++;
      hours.innerText = counterHours;

      if (counterHours < 10) {
        hours.innerText = `0${counterHours}`;
      }
      else if (counterHours <= 2) {
        counterHours = 0;//??як зробити щоб після певної кількості годин таймер спинявся
        clearInterval(interval);
      }
      // console.log(`counter = ${counter}`);
      // console.log(`counterMin = ${counterMin}`);
      // console.log(`counterHours = ${counterHours}`);
    }
  }
}

//завдання 2-ге на регулярні вирази

let form = document.createElement('form');
form.style.width = '670px';
form.style.height = '100px';
form.style.backgroundColor = 'lightyellow';
form.style.border = '2px solid darkblue';
form.style.margin = '0 auto';
form.style.marginTop = '10px';
form.style.marginBottom = '40px';
form.style.paddingTop = '1%';
container_stopwatch[0].before(form);

let input = document.createElement('input');
input.type = 'tel';
input.name = 'phone';
input.id = 'input';
input.placeholder = '093-123-45-67';
// input.value = '';
input.style.margin = '1% 3% 1% 15%';
form.append(input);

let label = document.createElement('label')
label.for = 'input';
label.innerHTML = 'Enter your phone number';
label.style.margin = '3% 20%';
form.prepend(label);

let button = document.createElement('button');
button.type = 'button';
button.id = 'button';
button.innerText = 'Confirm';
button.style.fontSize = '1rem';
button.style.width = '25%';
button.style.height = '30%';
button.style.marginleft = '.5%';
form.append(button);

let div = document.createElement('div');
div.style.width = '500px';
div.style.height = '70px';
// div.style.border = '3px solid darkgreen';
// div.style.backgroundColor = 'lightgreen';
div.margin = '20px';
div.style.color = 'red';
// div.style.visibility = 'hidden';
div.style.margin = '0 auto';
form.before(div);

let pattern = /0[0-9]{2}\-[0-9]{3}\-[0-9]{2}\-[0-9]{2}/;
// console.log(pattern);

button.onclick = () => {
  // console.log(input.value);
  if (pattern.test(input.value)) {
    div.innerText = null;
    input.style.border = null;

    input.style.border = '3px solid green';
    input.style.color = 'green';

    document.location = 'https://origamistudy.com/motyvujuchi-cytaty-dlja-dosjagnennja-uspihu/';
    // console.log(pattern.test(input.value));
  }
  else {
    input.style.border = null;
    button.style.backgroundColor = null;
    setTimeout(div.innerText = 'Спробуйте ще раз', 3000);
    // console.log(pattern.test(input.value));
  }
}