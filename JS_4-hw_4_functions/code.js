/*Функції
1.Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
Технічні вимоги:
- Вважати за допомогою модального вікна браузера два числа.
- Вважати за допомогою модального вікна браузера математичну операцію, яку потрібно здійснити. Сюди може бути введено `+`, `-`, `*`, `/`.
- Створити функцію, в яку передати два значення та операцію.
- Вивести у консоль результат виконання функції.
 */

var num1 = parseFloat(prompt("Enter first number"));
var num2 = parseFloat(prompt("Enter second number"));
var oper = prompt("Enter operation `+`, `-`, `*`, `/`");
// console.log(num1, num2, oper);
// document.write(`Number 1 = ${num1} Number 2 = ${num2} "<br>"`);

// функція для арифмет.операцій
var res;

function culc(a, b) {
    if (oper === "+") {
        res = num1 + num2;
    }
    if (oper === "-") {
        res = num1 - num2;
    }
    if (oper === "*") {
        res = num1 * num2;
    }
    if (oper === "/") {
        res = num1 / num2;
    }
    // return res;
    console.log(`Функції - завдання 1-ше : ${(res)} <br><hr/>`);
}
culc();
// console.log(parseFloat(res));
// document.getElementById("div").innerHTML(`Функції - завдання 1-ше : ${(res)} <br><hr/>`);


/*Необов'язкове завдання просунутої складності:
- Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при введенні вказав не числа - запитати обидва числа заново (при цьому значенням за умовчанням для кожної зі змінних повинна бути введена раніше інформація).
*/
// if((oper === "+") && ((num1 ==! Number) || (num2 ==! Number)))


/* 2.Напишіть функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.*/

let array = [10, 2, 4];


function fn(a) {
    res = a * a;
    return res;
}

function mapper(fn, array) {
    // let array = [2, 5, 6];
    
    // return array.map(fn);

    console.log(array.map(fn));
       
    // var newArr = [];
    // for (let i = 0; i < array.length; i++) {
    //     newArr = array[i].reverse;
    // }
    // document.write(newArr);
}

// function fn(a) {
//     res = a * a;
//     return res;
// }

mapper();


/* +- 3.Перепишіть функцію за допомогою оператора '?' або '||'
Наступна функція повертає true, якщо параметр age більше 18. В іншому випадку вона ставить питання confirm і повертає його результат.
function checkAge(age) {
if (age > 18) {
return true;
 else {
return confirm('Батьки дозволили?');} }
 */
// function checkAge(age) {
//     if (age > 18) {
//     return true;
//     } else {
//     return confirm('Батьки дозволили?');
//     }
// }

// тернарний оператор
// var age = 19;
function checkAge(age) {
    age > 18 ? true : confirm('Батьки дозволили?');
    // debugger
    return;
}
// var ageRes = checkAge(19);
checkAge(19);
console.log();
// document.write();

// оператор АБО ||
function checkAge1(age) {
   return (age > 18) == true || (confirm('Батьки дозволили?'));
    // return; //? не працює
}
checkAge1(11);
console.log();
