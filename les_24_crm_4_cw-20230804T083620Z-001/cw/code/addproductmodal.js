//поточний файл addproductmodal.js для відображення модального вікна після натиску на кнопку "додати продукт" на основній сторінці (після авторизації)

//імпорт даних з файлу functions.js, а саме ф-ції showForm
import { showForm } from "./functions.js";

const modal = document.querySelector(".container-modal");//отримуємо доступ до самого модального вікна
const modalBody = document.querySelector(".modal-body");//отримуємо доступ до тіла модального вікна, куди будуть виводитись дані 
const addProduct = document.querySelector("#btn-add-product");//отримуємо доступ до кнопки(папки) "додати продукт"
const modalClose = document.getElementById("modal-close");//отримуємо доступ до кнопки "закрити модальне вікно"
const productSelect = document.getElementById("product-select");//отримуємо доступ до кнопки "обрати категорію"

//додаємо прослуховувач подій addEventListener і після натискання на папку "додати продукт" видалити classList hide
addProduct.addEventListener("click", () => {
    modal.classList.remove("hide");
})

//додаємо прослуховувач подій addEventListener і після натискання на кнопку "закрити модальне вікно" додати classList hide
modalClose.addEventListener("click", () => {
    modal.classList.add("hide");
})

//додаємо прослуховувач подій addEventListener і після вибору певного option (change) на формі 
//з випадаючим списком викликати функцію showForm(), до якої параметрами передаємо значення івент таргет і приводимо до нижнього реєстру(e.target.value.toLowerCase())
productSelect.addEventListener("change", (e) => {
    //{ id, name, qantity, price, status, date, description} - за допомогою ксласу.
    //console.log(productSelect.value);
    showForm(e.target.value.toLowerCase())//виклик ф-ції з параметрами, яка показує форму з елементами на ній(інпутами) 
})