//у поточний файлі functions.js містяться функції

const buttonSend = document.querySelector("#submit");//ловимо кнопку для надсилання даних отриманих у формі

//функція showForm() яка показує/виводить форму з даними (інпутами для введення даних) в залежного від відбраного підпроекту(select)- тобто тут наповнення productBody
//експортуємо цю ф-цію (надаємо до неї доступ з іншого файла)
export function showForm(productType = "магазин") {
    sendBtnDisabled()//виклик ф-ції для деактивовації кнопки
    const productBody = document.querySelector(".product-body");//отримуємо достут до діва у формі, куди виводитимуться дані по кожному з підпроектів(ресторан, відео, магазин)
    productBody.innerHTML = "";//очищуємо форму
    // product-body 
    if (productType === "магазин") {//якщо productType === "магазин" то виводимо дані магазину
        productBody.append(...createStoreForm());//через метод append() викликаємо ф-цію яка створює елементи форми і через деструктуризацію дістаємо елементи масиву

    } else if (productType === "ресторани") {
        productBody.append(...createRestoranForm());


    } else if (productType === "відео хостинг") {
        productBody.append(...createVideoForm())

    }
    buttonSend.addEventListener("click", () => {//прослуховуач подій на подію click на кнопці buttonSend
        sendData(productType);//виклик ф-ції у параметрах передаємо productType
        document.querySelector(".container-modal").classList.add("hide");//і додаємо клас hide, чим і закрваємо вікно форми
    });

}

//функція createStoreForm() створює форму з елементами для магазину(повертає масив з цими даними)
function createStoreForm() {
    return [//масив який містить в собі три елементи для кожного інпута
        //виклик ф-ції createElementForm()до якої в якості аргументів передаємо дані
        createElementForm(//виклик функції для створення елементу (колонки з даними в таблиці) 
            "Введіть назву продукта",
            "text",
            "product-name",
            "текст має бути Українською",
            validate),
        createElementForm(
            "Введіть ціну",
            "number",
            "product-price",
            "тут мають бути лише числа"),
        createElementForm(
            "Опис товару",
            undefined,
            "product-description",
            "текст має бути Українською",
            test())
    ]
}

//ф-ція test вирішує що робити в парні і не парні дні
function test() {
    if (new Date().getDate() % 2 === 0) {
        return validate;
    } else {
        return alert
    }
}

//функція createRestoranForm() створює форму для ресторану (масив інформації)
function createRestoranForm() {
    return [//масив який містить в собі три елементи для кожного інпута
        createElementForm(
            "Введіть назву продукта",
            "text",
            "product-name",
            "текст має бути Українською"),
        createElementForm(
            "Введіть ціну",
            "number",
            "product-price",
            "тут мають бути лише числа"),
        createElementForm(
            "Опис товару",
            undefined,
            "product-description",
            "текст має бути Українською")
    ]
}

//функція createVideoForm() створює форму для відео-хостингу
function createVideoForm() {
    return [//масив який містить в собі два елементи для кожного інпута
        createElementForm(
            "Назва відео",
            "text",
            "video-name",
            "текст має бути Українською"),
        createElementForm(
            "Посилання на відео",
            "url",
            "video-url",
            "Вкажіть в форматі https://name.org")
    ]
}

//функція createElementForm() яка створює кожен елемент форми - ф-ція управління інпутами - шаблон
//у параметрах за замовчуванням placeholder(label), type(input), classInput, errorText(error), стрілкова функція eventInput=()=>
//функція eventInput=()=>відповідає за отримання даних введених в інпут і подальше їх опрацювання(за умовчанням (у параметрах) вказується для того щоб уникнути помилки, якщо наприклад не навісити оброблювальник подій на інпут або не вказати цю фуккцію
function createElementForm(placeholder = "", type = "text", classInput = "", errorText = "Помилка", eventInput = () => { }) {
    const parent = document.createElement("div"),//генеруємо елемент дів - не приймає контенту
        label = document.createElement("label"),//генеруємо елемент label
        input = document.createElement("input"),
        error = document.createElement("div");

    parent.classList.add("input");//батьківському діву додали клас .input
    label.innerText = placeholder;//у лайблі буде placeholder
    input.type = type;//тип інпута = той тип шо приходить (у об'єкта інпут є значення тайп що відповідає за атрибут тайп або через setAttribute)
    input.validate = false;//властивість валідейт на об'єкті інпут за умовчанням іє не валідною(false)
    input.classList.add(classInput);//якщо клас прийде порожнім то просто його проігнорувати(за замовч.пустий рядок-у параметрах)
    error.classList.add("error", "hide");//якщо буде помилка то додати классЛіст("error"), а classList "hide" щоб помилки були сховані поки не будуть потрібні
    error.innerText = errorText;//до div error додати errorText
    input.addEventListener("change", eventInput);//якщо відбудеться зміна якась на інпуті(подія "change") і ця подія викликає функцію eventInput=()=>
    input.addEventListener("input", (e) => {//на константу input вішається подія вводу даних(input) 
        eventInput(e.target.value, e.target.classList.value, error, input);//викликається ф-ція eventInput()-у якості першого аргументу отримує текст,
        //в якості другого арг.-знач.класліст, трет.аргум.-
    });
    input.addEventListener("focus", () => {//через прослуховувач подій і подію "focus"(фокус) на елементі (на інпуті)
        label.classList.add("none-placeholder")//якщо є ця подія то до лейбла додати classList який переміщує плейсхолдер з інпута на місце лейбла
    })
    input.addEventListener("blur", () => {//через прослуховувач подій і подію "blur"(втрата фокусу) на елементі (на інпуті)
        if (!input.value) { //якщо не має ніякого значення інпуту, тобто інпут пустий
            label.classList.remove("none-placeholder")//видалити classList "none-placeholder" що повертає лейбл на місце плейсхолдера
        }
    })

    parent.append(label, input, error);//за доп. методу append() додали до діва parent три елем.

    return parent //повертаємо parent
}
//ф-ція createId() для генерування id
function createId() {
    let symbols = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890";//символи які використовуються для генерування іd
    let passLength = 8;//довжина іd (не більше 8 символів)

    let string = "";//змінна до якої запишеться іd

    for (let i = 0; i < passLength; i++) {
        let n = Math.floor(Math.random() * symbols.length);
        string += symbols[n];
    }
    return string;
}

//клас для збереження даних магазина
class StoreProduct {
    constructor(name, price, description, date) {
        this.id = createId();
        this.name = name;
        this.price = price;
        this.status = false;
        this.date = date;
        this.description = description;
        this.category = this.constructor.name;
        this.quantity = "0";
    }
}
//клас для збереження даних ресторана
class RestProduct {
    constructor(name, price, description, date) {
        this.id = createId();
        this.name = name;
        this.price = price;
        this.status = false;
        this.date = date;
        this.description = description;
        this.category = this.constructor.name;
        this.quantity = "0";
    }
}
//клас для збереження даних відео-хостинга
class VideoProduct {
    constructor(name, url, date) {
        this.id = createId();
        this.name = name;
        this.url = url;
        this.date = date;
        this.category = this.constructor.name;
    }
}
//створення масивів (глобальних об'єктів), куди додаватимуться поля відповідних підпроетів з форми
let storeProducts = [];
let restProducts = [];
let videoProducts = [];

//функція для відправки даних після заповнення інпутів до локалсторейдж
function sendData(type) {
    let date = new Date();//отримання об'єкту поточна дата
    date = date.toLocaleString().split(",")[0].split(".").reverse().join(".") + date.toLocaleString().split(",")[1];//дата і час за тз
    if (type === "магазин") {
        const name = document.querySelector(".product-name").value;//значення відповідних інпутів
        const price = document.querySelector(".product-price").value;
        const desc = document.querySelector(".product-description").value;
        if (localStorage.storeProducts) {//якщо в локалсторейдж є масив з даними storeProducts
            storeProducts = JSON.parse(localStorage.getItem("storeProducts"));//то щоб після оновлення сторінки не стирались дані в локалсторейдж 
            //ми з локалсторейдж спочатку завантажуємо дані в масив, а потім додаємо наступні елементи до масиву(storeProducts.push(new StoreProduct(name, price, desc, date));)
        }
        storeProducts.push(new StoreProduct(name, price, desc, date));//потім записуємо нові дані з інпутів до масиву і отримуємо оновлений масив з усіма даними
        document.querySelector(".product-name").value = ""//чистимо інпути після додавання до локалсторейдж
        document.querySelector(".product-price").value = ""
        document.querySelector(".product-description").value = ""
        sendBtnDisabled()
    }
    else if (type === "ресторани") {
        const name = document.querySelector(".product-name").value;
        const price = document.querySelector(".product-price").value;
        const desc = document.querySelector(".product-description").value;
        if (localStorage.restProducts) {
            restProducts = JSON.parse(localStorage.getItem("restProducts"));
        }
        restProducts.push(new RestProduct(name, price, desc, date));
        localStorage.setItem("restProducts", JSON.stringify(restProducts));
        sendBtnDisabled()
    }
    else if (type === "відео хостинг") {
        const name = document.querySelector(".video-name").value;
        const url = document.querySelector(".video-url").value;
        if (localStorage.videoProducts) {
            videoProducts = JSON.parse(localStorage.getItem("videoProducts"));
        }
        videoProducts.push(new VideoProduct(name, url, date));
        localStorage.setItem("videoProducts", JSON.stringify(videoProducts));
        sendBtnDisabled()
    }
}

//ф-ція для валідації введених в інпути даних
const validate = (value, classList, error, input) => {
    //const [...inputs] = document.querySelectorAll("input");

    if (classList.includes("name")) {
        const patern = /^[а-яіїєґ0-9- ]{3,20}$/i;//патерн може містити літери української абетки, цифри, довжина від 3 до 20 символів
        if (patern.test(value)) {//??
            error.classList.add("hide");
            input.validate = true;
            //buttonShow()
        }
        else {
            input.validate = false
            error.classList.remove("hide");
        };


    }
    if (classList.includes("price")) {
        const patern = /^[0-9.]{1,6}$/i;
        if (patern.test(value)) {
            error.classList.add("hide");
            input.validate = true;
            //buttonShow()
        }
        else {
            error.classList.remove("hide");
            input.validate = false

        }

    }
    if (classList.includes("description")) {
        const patern = /^[а-яіїєґ0-9- ]{5,}$/i;
        if (patern.test(value)) {
            error.classList.add("hide");
            input.validate = true;
            //buttonShow()
        }
        else {
            error.classList.remove("hide");
            input.validate = false

        }
    }
    if (classList.includes("url")) {

        const patern = /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i;
        if (patern.test(value)) {
            error.classList.add("hide");
            input.validate = true;
            //buttonShow()
        }
        else {
            error.classList.remove("hide");
            input.validate = false
        }
    }
    buttonShow()
}

// Активація кнопки - щоб кнопка показувалась
function buttonShow() {
    const [...inputs] = document.querySelectorAll("input");//отримуємо всі діви
    const rez = inputs.every(a => {//перебираємо кожен елемент масиву-метод every() перевіряє чи повертають всі ел. масиву однакове значення
        return a.validate === true;//властивість кожного ел.validate має бути ідентичною тоді повертається true
    });

    if (rez) {
        buttonSend.disabled = false;
    } else {
        buttonSend.disabled = true;
    }
}

//ф-ція для деактивації кнопки
const sendBtnDisabled = () => {
    buttonSend.disabled = true;
}



