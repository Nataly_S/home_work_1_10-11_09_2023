//поточний файл store.js для виводу даних у магазин (для всіх підпроектів -магазину, відео-хост, ресторану)

//експортуємо ф-цію store()
//ф-ція store() яка формує теги - відмальовує таблицю, а якщо немає даних- показує модальне вікно що даних немає
export function store() {
    const table = document.querySelector("tbody");
    clearHTMLContent(table)
    //якщо в магазині в localStorage нічого не має або пустий масив
    if (!localStorage.storeProducts || localStorage.storeProducts === "[]") {//localStorage.storeProducts-масив з даними в локалсторейдж якщо буде undefind то не зайдемо,а якщо !localStorage.storeProducts (не undefind)-зайдемо
        //то отримуємо ел. по id і створюємо елемент, використовуючи insertAdjacentHTML, вбудовуючи перед закриваючим тегом
        document.getElementById("box-show").insertAdjacentHTML("beforeend", `<div class="box-none">Поки немає даних ❌</div>`)
        return;//return потрібен для обривання виконання даної функції у випадку якщо ніяких даних немає
    }

    //якщо товари є записуємо до products дані з localStorage
    //products(змінна з усією продукцією по магазину) містить дані у вигляді рядка, тому використовуємо JSON і метод parse() д/перетвор. у масив або об'єкт
    //products це масив інформації
    const products = JSON.parse(localStorage.storeProducts);

    //якщо продукти є перебираємо цей масив і викликаємо функцію createTableElement() і передаємо аргументи-ця ф-ція формує рядки і стовчики таблиці з відповідними даними
    products.forEach((product, index) => {//перебираємо масив 
        //тут створюється тег -ф-ція createTableElement() малює цілу таблицю (рідки і стовчики)
        createTableElement(index + 1, product, "line", editEvent, deleteEvent, table);
    });
    // Перевірка стореджа
}
//об'єкт readonly використовуємо коли потібно по власивостям цього об'єкта зробити певні дії(тільки для читання, не для редаг.)
const readonly = {
    id: "id",
    category: "category"
}

//ф-ція createTableElement() створює таблицю //і заносить елементи до таблиці
function createTableElement(number, product, className, editEvent, deleteEvent, table) {
    //з масиву інформації з кожного елементу потрібно зробити окремий рядок-цей масив потрібно перебрати за допомогою методів масиву
    //і на моменті кожної ітерації викликати ф-цію createTableElement() і передавати туди номер ітерації(number),об'єкт поточного продукту,
    //за необхідності вигадувати клас,передавати функцію-оброблювальник на редагування,ф-цію-обробл.на видалення
    //const { name, quantity, - це все звичайні змінні, не об'єкти. ці змінні у фігурн.дужках -це деструктуризація-будемо діставати дані з об'єкта
    //якщо властивості об'єкта будуть одноіменні зі змінними, то в такому випадку властивості об'єкта одноіменні запишуться до змінних
    // 
    const { name, quantity, price, status, description, date, category, url } = product;//з деструктуризацією об'єкт(product) працює так: якщо прийшла якась властивість{} то цю властивість записали до цього об'єкту
    const tr = document.createElement("tr");//створення рядка таблиці

    const editButton = document.createElement("button");//створ. кнопки редагувати
    editButton.innerText = "Редагувати";
    editButton.style.cursor = "pointer";
    editButton.addEventListener("click", () => {
        editEvent(product);//передаємо об'єкт продукту
    });

    //елемент таблиці кнопочка 
    const delButton = document.createElement("button");//створ. кнопки видалити
    delButton.innerText = "Видалити";
    delButton.style.cursor = "pointer";
    //коли виникає подія клік викликається ф-ція deleteEvent() і передає в якості аргументу продукт з властивостям name, quantity, price, status, description, date, category, url 
    delButton.addEventListener("click", () => {
        deleteEvent(product);//передаємо об'єкт продукту(product)
    });

    //якщо обраний магазин
    if (window.location.pathname.includes("store")) {
        const elements = [//створюється масив td(колонок)
            newTd(number),//викликом ф-ції newTd() передаємо значення (властивість) number// і отримуємо елемент td вже у функції newTd() 
            newTd(name),//отримуємо дані створюємо необхідні елементи у ф-ції
            newTd(quantity),//так само - створюємо відповідні елементи у ф-ції
            newTd(price),
            newTd(editButton),
            newTd(status ? "✅" : "❌"),//тернарним оператором перевіряємо статус-якщо true галочка,як false -хрестик,передаємо у ф-цію newTd() і отрим. рез-т
            newTd(date),
            newTd(delButton)
        ]
        tr.append(...elements);//і цей масив td(колонок) додаємо дочірнім елементом до tr(рядка)
    }
    //якщо обраний ресторан
    else if (window.location.pathname.includes("restoran")) {
        const elements = [
            newTd(number),
            newTd(name),
            newTd(quantity),
            newTd(price),
            newTd(editButton),
            newTd(status ? "✅" : "❌"),
            newTd(date),
            newTd(delButton)
        ]
        tr.append(...elements);
    }
    //якщо обраний відео-хостинг
    else if (window.location.pathname.includes("video")) {
        const elements = [
            newTd(number),
            newTd(name),
            newTd(date),
            newTd(url),
            newTd(editButton),
            newTd(delButton)
        ]
        tr.append(...elements);
    }
    table.append(tr);//і цей сформований tr (рядок) додаємо всередину таблиці
}

//задачею ф-ції newTd() пеевірити чи є властивість - якщо немає нічого не робить-виходимо з ф-ції,
//якщо є властивість то створити td з цією властивістю
//ф-ція приймає в якості аргументу props, перевіряє їх і виводить дані
function newTd(props) {

    if (!props) return;//якщо властивості не має, то вийти з функції і припинити її подальше виконання
    const td = document.createElement("td");
    if (props.localName === "button") {
        td.append(props);//в td всередину додаємо саму кнопку 
    }
    else {
        td.innerText = props;//в ін.випадках додаємо просто текст кнопки
    }
    return td;
}

//ф-ція для кнопки видалення елементів
function deleteEvent(product) {
    const { id } = product;//дістати властивість id з об'єкту product
    const allProducts = JSON.parse(localStorage.storeProducts);//дістаємо всі об'єкти з localStorage -рядок який у форматі JSON методом parse()отримуємо у вигляді звичайного об'єкту

    allProducts.splice(allProducts.findIndex(el => id === el.id), 1)//el => id === el.id-логіка для пошуку id= id порівнюється з іd елемента
    //(el => id === el.id)-перший аргумент індекс, другий аргумент один елемент(1)
    //allProducts = allProducts.length === 0? undefined : allProducts //перевірка є дані чи не має нічого в локалсторейдж(чи пустий масив) або перевірка що робимо у ф-ції store() (localStorage.storeProducts === "[]")
    localStorage.storeProducts = JSON.stringify(allProducts);//JSON.stringify(allProducts)-переводимо в рядок дані(продукт який знайшли за індексом і вирізали за доп.методу splice) і
    //записуємо до localStorage видозмінений масив(allProducts)
    store();//викликаємо ф-цію
}

//ф-ція editEvent() для редагування даних таблиці, ця ф-ція приймає продукт (масив даних) - повні об'єкти з описом їх властивостей 
//коли редагуємо дані в елементах то також запускається  ф-ція showPropertyProduct, яка показ.властивості об'єкту
function editEvent(product) {
    //name, quantity, price, status, description, date, category, url //звернення до змінної за властивостями
    let { id } = product;//з масиву product дістаємо елемент по id
    const allProducts = JSON.parse(localStorage.storeProducts);//дістаємо дані з локалсторейдж
    const [oldObj] = allProducts.splice(allProducts.findIndex(el => id === el.id), 1);//видалений об'єкт записали у змінну
    const modal = document.querySelector(".container-modal");//отримуємо доступ до моальногго вікна для редаг.даних
    const close = document.querySelector("#modal-close");//отримуємо доступ до кнопки "закрити"

    //відкривається модальне вікно
    modal.classList.remove("hide");//показується модальне вікно тому що прибираємо клас hide
    close.addEventListener("click", () => {//якщо на кнопці "закрити" ловимо подію клік то закрити модальне вікно
        modal.classList.add("hide");
    })
    showPropertyProduct(oldObj, allProducts, modal);//передаємо в парам. oldObj-той об'єкт який дістали з localStorage який будемо редаг
    //також оновлений масив allProducts і мод.вікна 
}

//ф-ція showPropertyProduct() для виведення списку властивостей продуктів
function showPropertyProduct(p, arr, modal) {
    const edit = document.getElementById("edit");//форма для виведення лейбел інпут(назва і поле для введ.даних)
    const props = Object.entries(p);//методом entries() дістаємо масиви із зовн.масиву, а саме список властивостей і їх значень
    const btnSave = document.createElement("button");//кнопка збереження даних після редаг.
    btnSave.type = "button";
    btnSave.innerText = "Зберегти"
    btnSave.addEventListener("click", () => {
        // 1
        const [...inputs] = document.querySelectorAll("#edit input");//перебираємо всі інпути щоб прочитати і записати оновлені дані після редаг. #edit-форма input-всі інпути у формі
        //console.log(inputs);
        //потрібно згенеруват новий об'єкт з даними які редаг і попередніми з полім які не редаг(id, category)
        const newObjectProduct = {

        }
        //по натиску кнопки btnSave має згенер.новий об'єкт з ключами і новими значеннями, крім тих які не змін.(попер дані-id, category)
        inputs.forEach((input) => {
            newObjectProduct[input.key] = input.value//запис.значення інпута до нового об'єкту з властивістю 
        })

        //перевірка статусу продукту-якщо кільк.більше 0 то true, інакше false()
        if (newObjectProduct.quantity > 0) {
            newObjectProduct.status = true;
        } else {
            newObjectProduct.status = false;
        }

        arr.push(newObjectProduct);//записали новий обєкт до масиву
        localStorage.storeProducts = JSON.stringify(arr);//записуємо до localStorage новий обєкт у вигляді рядка
        modal.classList.add("hide");
        store()
    })

    //до formData записуються діви, в яких вкладені чілдрени(лайбел та інпут)
    const formData = props.map(([a1, a2]) => {//[a1, a2] - ключ значення - відправляємо у ф-цію createPropertyElement() для створ.кожного ел.(елемент склад.з лейбла і інпута)
        return createPropertyElement(a1, a2)//повертаємо результ викон.ф-ції ключ і значення
    })

    clearHTMLContent(edit)
    edit.append(...formData, btnSave)
}

//ф-ція createPropertyElement() для створення елементів інпутів для редагування в окремому модальному вікні
//ф-ція коллбек fnEvent яка є в параментах на той випадок якщо потрібно буде створити таку ф-цію в подальшому(напр.,д/якихось дії над інпутами)
function createPropertyElement(name, value, fnEvent) {
    const id = Math.floor(Math.random() * 1000000);//створюється id у вмгляді випадкових чисел
    const div = document.createElement("div");//створ.div
    const label = document.createElement("label");
    const input = document.createElement("input");
    input.key = name;
    label.setAttribute("for", id);//звязуємо label з input
    input.id = id;
    input.value = value;
    label.innerText = name;

    input.addEventListener("click", () => {
        //fnEvent()
    })

    //встановлюємо правило якщо поля id і category -то тільки для читання
    if (name === readonly.id || name === readonly.category) {
        input.readOnly = true;//завдяки властивості readOnly - цей інпут тільки для читання(не редаг.)
        //input.disabled = true
    }
    //якщо name властивості status повернути пусто
    if (name === "status") {
        return ""//повер.пустий рядок
    }

    div.append(label, input);//додаємо в середину діва модально вікна label та input
    return div;//повертаємо цей дів
}

//ф-ція clearHTMLContent() для очистки контенту форми-після видалення або редагування елементів оновлюється сторінка форми
function clearHTMLContent(el) {
    if (typeof el !== "object") return;//якщо тип елементу не об'єкт то нічого не робимо
    el.innerHTML = "";//а якщо прих.ел, то робимо для нього порожній рядок
}

