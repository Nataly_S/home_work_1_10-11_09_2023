//поточний файл authoresation.js - тут міститься код для сторінки авторизації(перевірка чи користувач є авторизованим, перевірка правильності введених даних)

//функція isAuth() яка показує сторінку авторизації (якщо в локалсторадж є дані), тобто чи користувач авторизований
function isAuth() {
    if (localStorage.isAuth) {//перевірка чи є користувач авторизований
        return//то виходимо з ф-ції
    } else if (!location.pathname.includes("authorization")) {//якщо не містить даних у локалсторейдж (НЕ локешен-шлях-містить файл авторизація) 
        location = "/authorization"//то переводимо на сторінку авторизації
    }
}

//блок try catch потрібен для того щоб уникнути виведення помилки, яка виникає після авторизації
//тому що після того як користувач авторизувався, він перенаправляється на сторінку основну,
//а помилка виникає тому що файл authorization підключений до основного файлу index.js (викликом функції isAuth()), і цей основний файл є підключеним до всіх сторінок
//блок try буде перевіряти увесь код і якщо код матиме помилку, то блок try зламається, але це не вплине на роботу сторінки
//якщо виникне помилка, то вона отмується у блок catch 
try {
    console.log((new Date().toLocaleDateString("en", { weekday: "long" }) + new Date().getHours()).toLocaleLowerCase());
    console.log(new Date().toLocaleDateString("uk", { weekday: "long" }).toLocaleLowerCase() + new Date().getMinutes());

    //функція Auth() яка перевіряє правильність введених користувачем даних під час авторизації
    function Auth() {
        const loginData = (new Date().toLocaleDateString("en", { weekday: "long" }) + new Date().getHours()).toLocaleLowerCase(); //отримуємо потрібну дату для логіна - назву дня тижня англійською + поточна година
        const passwordData = new Date().toLocaleDateString("uk", { weekday: "long" }).toLocaleLowerCase() + new Date().getMinutes(); //отримуємо потрібну дату для пароля - назву дня тижня українською + поточні хвилини

        //перевірка на ідентичність значення inputLogin і loginData та значення inputPassword i inputPassword
        if (inputLogin.value === loginData && inputPassword.value === passwordData) {
            localStorage.isAuth = true;//якщо введені дані є правильними то до локалсторадж запишеться true
            inputLogin.classList.remove("error");//видалити класліст еррор для логіна
            inputPassword.classList.remove("error");//видалити класліст еррор для пароля
            location = "/" //перенаправлення на головну сторінку (з підпроектами)
        } else {//інакше якщо дані вддені не вірні пісвітити поля ввдоду даних
            inputLogin.classList.add("error") //додати класліст еррор для логіна
            inputPassword.classList.add("error")//додати класліст еррор для логіна
        }
    }

    const btn = document.querySelector("#btn");//отримуємо доступ до кнопки

    const inputLogin = document.querySelector("[data-type='login']");//по атрибуту отримуємо доступ до інпуту для логіна
    const inputPassword = document.querySelector("[data-type='password']");//по атрибуту отримуємо доступ до інпуту для пароля

    //підключаємо прослуховувач до подій поля інпут логіна (подія change - на зміну значення полі вводу)
    inputLogin.addEventListener("change", () => {
        btnShow();//викликаємо функцію btnShow(), яка робить кнопку активною або ні
    })
    //підключаємо прослуховувач подій до поля інпут пароля
    inputPassword.addEventListener("change", () => {
        btnShow();
    })
    //підключаємо прослуховувач подій до кнопки 
    btn.addEventListener("click", () => {
        Auth()//виклик функції, яка перевіряє чи авторизований користувач
    })

    //функція btnShow() перевіряє чи є значення в інпутах => якщо значення інпутів не пусті то зробити кнопку активною,
    //інакше(якщо значення пусті) кнопка не активна
    //ця функція не перевіряє правильність введених даних (тільки чи є введені якісь дані чи ні)
    function btnShow() {
        if (inputLogin.value !== "" && inputPassword.value !== "") {
            btn.disabled = false;
        } else {
            btn.disabled = true;
        }
    }

} catch (e) {
    //console.error(e);//виводить помилку, але це не ламає роботу сторінки
    //console.warn(e);//не виводить помилки, є просто попередження
}
//експортуємо функцію isAuth до основного файлу (index.js)
export { isAuth }