let list = document.getElementById('list');
// console.dir(list);
let selectedElem = '';
let counter;

//clear styles function
function clearCheckedElem() {
  if (selectedElem.className === 'checked') {
    selectedElem.classList.remove('checked');
  }
  // else if (selectedElem === true) {
  //   selectedElem.classList.remove('checked');
  // }
}

//+first element
function selectFirstChild() {
  clearCheckedElem();
  selectedElem = list.firstElementChild;//до selectedElem записуємо перший елемент у списку
  selectedElem.className = 'checked';//selectedElem призначаємо className = checked
}

//+last element
function selectLastChild() {
  clearCheckedElem();
  selectedElem = list.lastElementChild;
  selectedElem.className = 'checked';
}

//+next element
function selectNextNode() {
  clearCheckedElem();

  if (!selectedElem) {//коли не обраний жоден елемент
    selectedElem = list.firstElementChild;//записати до selectedElem перший елемент списку
  }
  else if (selectedElem === list.lastElementChild) {
    selectedElem = list.firstElementChild;
  }
  else {
    selectedElem = selectedElem.nextElementSibling;
  }
  selectedElem.classList.add('checked');
}

//+previous elem
function selectPrevNode() {
  clearCheckedElem();

  if (!selectedElem) {
    selectedElem = list.lastElementChild;
  }
  else if (selectedElem === list.firstElementChild) {
    selectedElem = list.lastElementChild; //list.previousElementSibling;
  }
  else {//if (selectedElem) {
    selectedElem = selectedElem.previousElementSibling;
  }
  selectedElem.className = 'checked';
  // if (selectedElem) {
  //   console.log(selectedElem);
  // }
}

//+create new child item
function createNewChild() {
  clearCheckedElem();//??не вдається прибрати клас checked - тобто зробити щоб жоден елемент не був обраним
  let newItem = document.createElement("li");
  list.appendChild(newItem);

  counter = 1;
  [...list.children].forEach((el) => {
    el.innerHTML = `List Item ${counter++}`;
    // el.style.color = 'darkGreen';
  });
  // !selectedElem;
  if (selectedElem) {
    console.log(selectedElem);
  }
  else if (!selectedElem) {
    console.log('-');
  }
}

//+remove created child elem
function removeLastChild() {
  clearCheckedElem();//??не вдається прибрати клас checked-зробити щоб жоден елемент не був обраним
  list.removeChild(list.lastElementChild);
}

//+create new child at start
function createNewChildAtStart() {
  clearCheckedElem();//??не вдається прибрати клас checked-зробити щоб жоден елемент не був обраним

  if (selectedElem) {
    console.log(selectedElem);
  }
  else if (!selectedElem) {
    console.log('-');
  }
  let newItem = document.createElement("li");
  list.prepend(newItem);

  counter = 1;
  [...list.children].forEach((el) => {
    el.innerHTML = `List Item ${counter++}`;
    // el.style.color = 'orange';
  });
  //   console.log(selectedElem);
  // }
  // else if (!selectedElem) {
  //   console.log('-');
  // }
}


