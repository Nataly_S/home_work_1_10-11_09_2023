//task 1 - Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.

let div1 = document.querySelector('.div-1');
// console.log(div1.firstElementChild.value);
// console.log(div1.children[1].value);

let button = document.querySelector('.btn-1');

button.onclick = () => {
  let i = 0;
  i = div1.firstElementChild.value;
  console.log(i);
  div1.firstElementChild.value = div1.children[1].value;
  div1.children[1].value = i;
}

//task 2 - Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір фону.

let section = document.querySelector('section');

for (let i = 0; i < 5; i++) {
  let divs1 = document.createElement('div');
  divs1.className = 'divs1';
  divs1.textContent = `div ${i + 1}`;
  divs1.style.width = '200px';
  divs1.style.height = '200px';
  // divs1.style.backgroundColor = `rgb(${random(255)},${random(255)},${random(255)})`;
  divs1.style.backgroundColor = 'blue';
  divs1.style.marginTop = '10px';
  divs1.style.marginBottom = '10px';
  section.append(divs1);
}

let divTags = document.getElementsByTagName('div');
console.log(divTags);

[...divTags].forEach((elem) => {
  // elem.style.backgroundColor = 'darkblue';
  elem.style.backgroundColor = `rgb(${random(255)},${random(255)},${random(255)})`;
});

function random(num) {
  // return Math.floor(min + Math.random() * (max + 1 - min));
  return Math.floor(Math.random() * (num + 1));
}

//task 3 - Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна
//згенерувати тег div з текстом, який був у багаторядковому полі. багаторядкове поле слід очистити після
//переміщення інформації

let textArea = document.querySelector('.textarea');
// console.log(textArea.value);

let button2 = document.querySelector('.btn-2');

button2.onclick = () => {

  let div = document.createElement('div');
  div.innerHTML = textArea.value;

  let section2 = document.body.children[1];
  // console.log(section2);

  section2.append(div);
  textArea.value = '';

}

//task 4 :
/**Створіть картинку та кнопку з назвою "Змінити картинку"
      зробіть так щоб при завантаженні сторінки була картинка
      https://itproger.com/img/courses/1476977240.jpg
      При натисканні на кнопку вперше картинка замінилася на
      https://itproger.com/img/courses/1476977488.jpg
      при другому натисканні щоб картинка замінилася на
      https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png */

let img = document.createElement('img');
// console.log(img);
window.onload = function () {

  img.src = "https://itproger.com/img/courses/1476977240.jpg";
  img.style.width = '200px';
  img.style.height = 'auto';

  let section2 = document.body.children[2];
  section2.prepend(img);
}

let button3 = document.querySelector('.btn-3');
// console.log(button3);
let count = 0;

button3.onclick = () => {
  count++;
  if (count == 1) {
    img.src = "https://itproger.com/img/courses/1476977488.jpg";
  }
  else if (count == 2) {
    img.src = 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png';
  }
}

//task 5 : Створює на сторінці 10 парахрафів і зробіть так, щоб при натисканні на параграф він зникав

let section3 = document.body.children[3];

for (let i = 0; i < 10; i++) {
  let p = document.createElement('p');
  p.innerText = `параграф ${i + 1}`;
  p.className = `paragraph ${i}`;
  p.style.backgroundColor = 'lightblue';
  section3.prepend(p);

  p.onclick = () => {
    p.remove();
  }
}




