//1.+Дані два масиви: ['a', 'b', 'c'] та [1, 2, 3]. 
//Об'єднайте їх разом.
document.write("Task 1" + "<br>");

let arrFirst = ['a', 'b', 'c'];
let arrSecond = [1, 2, 3];

let resTwoArrays = arrFirst.concat(arrSecond);
document.write(resTwoArrays.join(", ")); //a,b,c,1,2,3 
document.write("<hr/>");

//2.+Дан масив ['a', 'b', 'c']. 
//Додайте йому до кінця елементи 1, 2, 3.
document.write("Task 2" + "<br>");

let arrTwo = ['a', 'b', 'c'];
let resAdd = arrTwo.concat(1, 2, 3);
document.write(resAdd);
document.write("<hr/>");

//3.+ Дан масив [1, 2, 3]. Зробіть із нього масив [3, 2, 1].
document.write("Task 3" + "<br>");

let arrThree = [1, 2, 3];
let resReverse = arrThree.reverse();
document.write(resReverse.join(", "));
document.write("<hr/>");

//4.+ Дан масив [1, 2, 3]. 
//Додайте йому до кінця елементи 4, 5, 6.
document.write("Task 4" + "<br>");

let arrFour = [1, 2, 3];

document.write("elements of array in start : " + arrFour.join(", ") + "<br>");
document.write("size of array : " + arrFour.length);

document.write("<br>");

let resFourPush = arrFour.push(4, 5, 6);
document.write("quantity of elements after adding elements by push() method: " + resFourPush);

document.write("<br>");

document.write("array of elements in result : " + arrFour.join(", "));
document.write("<hr/>");

//5.+ Дан масив [1, 2, 3]. Додайте йому на початок 
//елементи 4, 5, 6.
document.write("Task 5" + "<br>");

const arrFive = [1, 2, 3];
document.write("quantity of array elements : " + arrFive);

document.write("<br>");

let resFiveShift = arrFive.unshift(4, 5, 6);
document.write(`array after adding elements in start of array by unshift() method : ${resFiveShift}`);

document.write("<br>");

document.write(`array elements in result :  ${arrFive.join(", ")}`);

document.write("<hr/>");

// 6.+Дан масив ['js', 'css', 'jq']. Виведіть перший елемент на екран.

document.write("Task 6" + "<br>");

const arrSix = ['js', 'css', 'jq'];
document.write(arrSix[0]);

document.write("<hr/>");

//7.+Дан масив [1, 2, 3, 4, 5]. За допомогою методу slice запишіть нові елементи [1, 2, 3].

document.write(`Task 7 <br>`);

let arrSeven = [1, 2, 3, 4, 5];
let resSevenSlice = arrSeven.slice(0, 3);

document.write(resSevenSlice.join(", "));
document.write("<hr/>");

//8.+ Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 4, 5].
document.write(`Task 8 <br>`);

const arrEight = [1, 2, 3, 4, 5];
document.write(`вивід початкового масиву : ${arrEight.join(", ")}<br>`);

let resEightSplice = arrEight.splice(1, 2);

document.write(`вивід елементів масиву, видалених за допомогою методу splice() : ${resEightSplice.join(", ")}<br>`);

document.write(`вивід масиву тепер : ${arrEight.join(", ")}`);
document.write("<hr/>");

//9.+Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 2, 10, 3, 4, 5].
// debugger
document.write(`Task 9 <br>`);

var arrNine = [1, 2, 3, 4, 5];
document.write(`вивід початкового масиву : ${arrNine.join(", ")}<br>`);

var resNineSplice = arrNine.splice(2, 1, 10);
document.write(`вивід елементів масиву, видалених за допомогою методу splice() : ${resNineSplice} <br>`);

document.write(`вивід елементів масиву після видалення і додавання елементів: ${arrNine.join(", ")}`);

document.write("<hr/>");

//10.+Дан масив [3, 4, 1, 2, 7]. Відсортуйте його.
document.write(`Task 10 <br>`);

var arrTen = [3, 4, 1, 2, 7];
document.write(`масив : ${arrTen.join(", ")}<br>`);

var resTenSort = arrTen.sort();
document.write(`вивід елементів масиву після сортування методом sort() : ${resTenSort.join(", ")}`);

document.write("<hr/>");

// 11.+? Дан масив з елементами 'Привіт, ', 'світ' і '!'. Потрібно вивести на екран фразу 'Привіт, мир!'.

document.write(`Task 11 <br>`);

let arrEleven = ['Привіт, ', 'світ', "!"];
//?чому коми виводяться, які між елементами масиву? - 
//через це ставила плюси для 
//конкатенації і застосувала метод join() на 127 рядку ""? 

document.write(`масив : ${arrEleven.join("")} <br>`);
// debugger
let resElevenSplice = arrEleven.splice(1, 1, 'мир');
document.write(`вивід елементів масиву, видалених за допомогою методу splice() : ${resElevenSplice}<br>`);

document.write(`вивід елементів масиву після видалення і додавання елементів : ${arrEleven.join("")}`);
document.write("<hr/>");

// 12.+Дан масив ['Привіт, ', 'світ', '!']. Необхідно 
//записати в нульовий елемент цього масиву слово 'Поки, '
// (тобто замість слова 'Привіт, ' буде 'Поки, ').
document.write(`Task 12 <br>`);

let arrTwelve = ['Привіт, ', 'світ', '!'];
document.write(`масив : ${arrTwelve.join("")}<br>`);

let resTwelweSplice = arrTwelve.splice(0, 1, 'Поки, ');
document.write(`вивід елементів масиву, видалених за допомогою методу splice() : ${resTwelweSplice.join("")}<br>`);

document.write(`вивід елементів масиву після видалення і додавання елементів : ${arrTwelve.join("")}`);
document.write("<hr/>");

//13.+Створіть масив arr з елементами 1, 2, 3, 4, 5 двома різними способами.
document.write(`Task 13 <br>`);

//перший спосіб
const arrThirteen1 = [1, 2, 3, 4, 5];
document.write(
    `перший спосіб виведення масиву:
    <br>
    const arrThirteen1 = [1, 2, 3, 4, 5]; 
    <br> 
    масив ${arrThirteen1.join(", ")}
    <br>
    <br>`
);

//другий спосіб
let arrThirteen2 = new Array(5);
arrThirteen2 = [1, 2, 3, 4, 5];
document.write(
    `другий спосіб виведення масиву:
    <br>
    let arrThirteen2 = new Array(5);
    <br>
    arrThirteen2 = [1, 2, 3, 4, 5];
    <br>
    масив ${arrThirteen2.join(", ")}`
);
document.write("<hr/>");

//14. + Дан багатовимірний масив arr: 
//var arr = [
//     ['блакитний', 'червоний', 'зелений'],
//     ['blue', 'red', 'green'],
// ];
//Виведіть за його допомогою слово 'блакитний' 'blue'
document.write(`Task 14 <br>`);
var arr = [
    ['блакитний', 'червоний', 'зелений'],
    ['blue', 'red', 'green'],
];
document.write((arr[0][0]) + ", " + (arr[1][0]));
document.write("<hr/>");

//15.+ Створіть масив arr = ['a', 'b', 'c', 'd'] і за 
//допомогою його виведіть на екран рядок 'a+b, c+d'.
document.write(`Task 15 <br>`);

let arrFift = ['a', 'b', 'c', 'd'];

document.write(`'${arrFift[0]}+${arrFift[1]}, ${arrFift[2]}+${arrFift[3]}'`);
document.write("<hr/>");

//16.+? Запитайте у користувача кількість елементів масиву. 
//Виходячи з даних, які ввів користувач створіть масив 
//на ту кількість елементів, яку передав користувач.
//у кожному індексі масиву зберігайте чило який 
//показуватиме номер елемента масиву.

//17.+ Зробіть так, щоб з масиву, який ви створили вище, 
//вивелися всі непарні числа в параграфі, а парні в спані 
//з червоним тлом.
document.write(`Task 16 - 17 <br>`);

let arrayQuantityByUser1 = prompt("Добрий день. Завдання 16. Будь ласка введіть кількість елементів масиву.(цикл for)");

document.write(`Виводимо масив чисел за вказаною кількістю : [`);
document.write('<br>');      

//реалізація завдання 16 - цикл for()
let i;
for (i = 0; i <= arrayQuantityByUser1; i++) {
    // let iComa = i.parceInt().join(", ");// ?не вийшло додати кому між елементами - методом join()

    if (i < arrayQuantityByUser1) {
        // [i.length-1];
        if (i % 2 == 0) {
            document.write(`<span style = color:red;>${i}</span>${','}`);
        } else {
            document.write(`<p>${i}${','}</p>`);
        }
    } else if (i == arrayQuantityByUser1) {
        if (i % 2 == 0) {
            document.write(`<span style = color:red;>${i}</span>`);
        } else {
            document.write(`<p>${i}</p>`);
        }
        // document.write(i);
    }
}
document.write(`]`);

//реалізація завдання 16 - цикл while()
// let arrayQuantityByUser2 = prompt("Добрий день. Завдання 16. Будь ласка введіть кількість елементів масиву.(цикл while)");

// document.write(`Виводимо масив чисел за вказаною Вами кількістю : [`);

// let j = 0;
// while(j <= arrayQuantityByUser2){
//     if (j < arrayQuantityByUser2){
//         document.write(j + ", ");
//     } else if(j == arrayQuantityByUser2){
//         document.write(j);
//     }
//     // document.write(j);
//     j++;
// }
// document.write(`]`);

document.write("<hr/>");

//18.+? Напишіть код, який перетворює та об'єднує всі 
//елементи масиву в одне рядкове значення. Елементи 
//масиву будуть розділені комою. 
document.write(`Task 18 <br>`);

var vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];//?чому коми, які є між елементами, виводятьcся під час виводу елементів масиву?
//через такий вивід використала метод join()
let str = vegetables.join(", ").toString();

document.write(str); // "Капуста, Ріпа, Редиска, Морквина"
document.write("<hr/>");
