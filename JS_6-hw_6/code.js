/*
+Створити конструктор Animal та +розширюючі його конструктори Dog, Cat, Horse.

+Конструктор Animal містить змінні food, location і +методи makeNoise, eat, sleep. 
+Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".

+Dog, Cat, Horse перевизначають методи makeNoise, eat.

+Додайте змінні до конструкторів Dog, Cat, Horse, що характеризують лише цих тварин.

+Створіть конструктор Ветеринар, у якому визначте метод який виводить в консоль treatAnimal(Animal animal). 
--Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.

+-У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. 
--У циклі надсилайте їх на прийом до ветеринара.
*/

//конструктор Animal
function Animal() {
    this.food = 'їжу для тварин';
    this.location = 'у дворі';
};

//три методи конструктора Animal
Animal.prototype.makeNoice = function () {
    console.log("Така тварина спить"); //? ці методи не виводяться в консоль -так має бути?
    // document.write("Така тварина спить");
};

Animal.prototype.eat = function (){
    console.log("eating");
};
Animal.prototype.sleep = function () {
    console.log("sleeping");
};

//розширюючі конструктори конструктора Animal
function Dog(){
  this.name = 'собака Джек';
//   this.location = location;
};

//новий екземпляр Dog конструктора Animal
Dog.prototype = new Animal(); //

//перевизначення методів makeNoise, eat.
Dog.prototype.makeNoice = function(){
    console.log(`${this.name} гавкає ${this.location}`);//this.location який має успадковувати від конструктора Animal не виводиться?
};
Dog.prototype.eat = function(){
    console.log(`${this.name} споживає ${this.food}`);//this.food який має успадковувати від конструктора Animal не виводиться?
};

// Dog.prototype.sleep = function (){
//     console.log("sleeping....");
// }

//розширюючий конструктор конструктора Animal
function Cat(name){
    this.name = 'кіт Мурчик';
};

//новий екземпляр Сat конструктора Animal
Cat.prototype = new Animal();

//перевизначення методів makeNoise, eat.
Cat.prototype.makeNoice = function (){
    console.log(`${this.name} муркотить ${this.location}`); // чи правильно ця this.location? - потрібно щоби брались дані з контруктора Animal - не показуються перевизначені дані -> тому що потрібно вказувати властивості в екземплярі класу function Cat()?
};
Cat.prototype.eat = function (){
    console.log(`${this.name} гамає ${this.food}`);
};

//розширюючий конструктор конструктора Animal
function Horse(){
    this.name = 'кінь Лис';
    // this.location = "біля річки";
    this.food = "їсть траву";
};

// новий екземпляр Horse конструктора Animal
Horse.prototype = new Animal();

//перевизначення методів makeNoise, eat.
Horse.prototype.makeNoice = function (){
    console.log(`${this.name} ірже ${this.location}`);//потрібно щоби брались перевизначені дані +
};
Horse.prototype.eat = function (){
    console.log(`${this.name} ${this.food}`);
};
// Horse(this.name, this.location, this.food);

const cat = new Cat();// cat - це екземпляр КЛАСУ
const doggie = new Dog();// doggie - екземпляр класу
const horse_nice = new Horse();//horse_nice - екземпляр класу

cat.makeNoice();
doggie.eat();

// document.write(new Cat("nameoo"));
// Cat.makeNoice();
// Cat.eat();
// Cat.sleep();

// метод Main

// Animal.prototype.main = function(){
//     let arrAnimal = Animal.prototype(new Dog, new Cat, new Horse);
// };

// конструктор Ветеринар
function PetDoc(){

};

// метод конструктора Ветеринар-лікаря, який лікує тварин
PetDoc.prototype.treatAnimal = function (animal){ // тварина, яка потрапила на обстеження до лікаря  
      
    console.log(Object.values(animal));// ? прочитати про метод Object.values()- на відео? який дозволяє вивести значення, які записані у вигляді масиву-тобто масив інформації від об'єкта, з тими властивостями, які у ньому записані
};

// метод main, де створений масив з усіма тваринами 
function main (){
    let arrAnimal = [new Dog(), new Cat(), new Horse()];

// У циклі тварини надсилаються на прийом до ветеринара:
    arrAnimal.forEach(element => {
        new PetDoc().treatAnimal(element)// оцей (element)  -> .treatAnimal(element) - це перебір кожного екземпляру класу у функції treatAnimal
    });
}
main();





